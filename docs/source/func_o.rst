Functions overview
==================

xarray_symTensor2d xr.DataArray accessor
****************************************

.. automodule:: xarray_symTensor2d.xarray_symTensor2d
	:special-members:
	:members:

